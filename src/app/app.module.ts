import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { AppComponent } from './app.component';
import { ProfilePage } from './pages/profile/profile.page';
import { ProductsPage } from './pages/products/products.page';
import { LoginPage } from './pages/login/login.page';
import { AdminDashboardPage } from './pages/admin-dashboard/admin-dashboard.page';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { RefreshTokenHttpInterceptor } from './interceptors/refresh-token-http.interceptor';
import { HttpAuthInterceptor } from './interceptors/auth-http-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    ProfilePage,
    ProductsPage,
    LoginPage,
    AdminDashboardPage,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule

  ],
  providers: [
      // Check Token Before each request
      {
        provide: HTTP_INTERCEPTORS,
        useClass: RefreshTokenHttpInterceptor,
        multi: true,
      },
      // Add HttpAuthInterceptor - Add Bearer Token to request
      {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpAuthInterceptor,
        multi: true
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
