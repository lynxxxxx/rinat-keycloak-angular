import { NgModule } from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from "./guards/auth.guard";
import { RoleGuard } from "./guards/role.guard";
import { AdminDashboardPage } from "./pages/admin-dashboard/admin-dashboard.page";
import { LoginPage } from "./pages/login/login.page";
import { ProductsPage } from "./pages/products/products.page";
import { ProfilePage } from "./pages/profile/profile.page";

const routes: Routes = [
    {
      path: '',
      component: LoginPage, // Public
    },
    {
      path: 'products',
      component: ProductsPage, // Public
    },
    {
      path: 'profile', 
      component: ProfilePage,
      canActivate: [AuthGuard],// Protected by Keycloak auth/ för alla roler
    },
    {
      path: 'admin/dashboard', 
      component: AdminDashboardPage,
      canActivate: [RoleGuard],// Protected by Keycloak auth // bare för admin
      data: {
        role: "Admin"
      }    
    }
  ];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
  })

export class AppRoutingModule {}